class WebController {
  home = (req, res) => {
    res.render("index", { content: "./content/home" });
  };
}

module.exports = WebController;
