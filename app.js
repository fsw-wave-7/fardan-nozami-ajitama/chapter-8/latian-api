const express = require("express");
const app = express();
const port = 3000;
const web = require("./routes/web");

app.set("view engine", "ejs");
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static("public"));

app.use(web);

app.use((req, res) => {
  res.status(404).send("halaman tidak ditemukan");
});

app.use((err, req, res) => {
  res.status(500).send("internal Server Error");
});

app.listen(port, () => {
  console.log(`server running at http://localhost:${port}`);
});
