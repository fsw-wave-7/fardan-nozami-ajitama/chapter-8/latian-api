const web = require("express").Router();
const WebController = require("../controllers/WebController");
const webController = new WebController();

web.use("/", webController.home);

module.exports = web;
